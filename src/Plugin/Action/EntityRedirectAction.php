<?php

declare(strict_types=1);

namespace Drupal\vbo_action_kit\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsPreconfigurationInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Action for test purposes only.
 *
 * @Action(
 *   id = "entity_redirect",
 *   label = @Translation("Redirect to a specified URI with entity IDs"),
 *   type = "",
 * )
 */
final class EntityRedirectAction extends ViewsBulkOperationsActionBase implements ViewsBulkOperationsPreconfigurationInterface {

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface[] $objects
   */
  public function executeMultiple(array $objects) {
    // Pass the configuration value to static finished method argument.
    if (!\array_key_exists('redirect_uri_base', $this->context['results'])) {
      $this->context['results']['redirect_uri_base'] = $this->configuration['redirect_uri_base'];
    }

    // Add entity IDs to the same results argument.
    foreach ($objects as $entity) {
      $this->context['results']['entity_ids'][$entity->id()] = $entity->id();
    }

    // Return a required message that'll serve no purpose in this case.
    return [['message' => '']];
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $this->executeMultiple([$entity]);
  }

  /**
   * {@inheritdoc}
   */
  public function setContext(array &$context): void {
    parent::setContext($context);

    // We need to be able to modify results from the action as well.
    $this->context['results'] = &$context['results'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildPreConfigurationForm(array $form, array $values, FormStateInterface $form_state): array {
    $form['redirect_uri_base'] = [
      '#title' => $this->t('Redirect URI base'),
      '#description' => $this->t('The action will redirect to this uri suffixed with entity IDs separated by commas (could be another view or controller arguments). No initial or trailing slash needed.'),
      '#type' => 'textfield',
      '#default_value' => $values['redirect_uri_base'] ?? '',
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $object->access('view', $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public static function finished($success, array $results, array $operations): RedirectResponse {
    // We don't want to show any messages, just redirect.
    // Convert to absolute first.
    $uri = \trim($results['redirect_uri_base'], '/') . '/' . \implode(',', $results['entity_ids']);
    $url = Url::fromUri("base:/$uri", ['absolute' => TRUE]);
    return new RedirectResponse($url->toString());
  }

}
